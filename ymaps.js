(function ($, window, document, undefined) {
    "use strict";

    if (!$) {
        return;
    }

    var ymapsDeferred = $.Deferred(),
        ymapsLoader = ymapsDeferred.promise();

    window.ymapsLoader = ymapsLoader;

    if (typeof ymaps == 'undefined') {
        $.getScript('https://api-maps.yandex.ru/2.1/?lang=ru_RU', function () {
            ymaps.ready(function () {
                ymapsDeferred.resolve();
            });
        });
    } else {
        ymaps.ready(function () {
            ymapsDeferred.resolve();
        });
    }


    function LocationSelector() { this.initialize.apply(this, arguments) }
    LocationSelector.prototype = {
        draggable: false,
        placemark: null,
        placemarkProperties: {},

        initialize: function (elem) {
            this.elem = elem;
            this.map = this.createMap();
        },

        createMap: function () {
            var id = 'map-' + Math.round(Math.random() * 9999),
                deferred = $.Deferred();

            if (!this.elem.attr('id')) {
                this.elem.attr('id', id);
            } else {
                id = this.elem.attr('id');
            }

            ymapsLoader.done(function () {
                var map = new ymaps.Map(id, {
                    center: [LocationSelector.center.latitude, LocationSelector.center.longitude],
                    controls: ['smallMapDefaultSet'],
                    zoom: LocationSelector.center.zoom
                });

                deferred.resolve(map);
            });

            return deferred.promise();
        },

        setDraggable: function (draggable) {
            this.draggable = !!draggable;

            if (this.placemark) {
                this.placemark.properties.set('draggable', true);
            }
        },

        setPosition: function (latitude, longitude, zoom) {
            var _this = this,
                deferred = new $.Deferred();

            if (!zoom) {
                zoom = LocationSelector.center.zoom;
            }

            if (!longitude) {
                longitude = LocationSelector.center.longitude;
            }

            if (!latitude) {
                latitude = LocationSelector.center.latitude;
            }

            this.map.done(function (map) {
                if (!_this.placemark) {
                    var properties = {};
                    for (var i in _this.placemarkProperties) {
                        properties[i] = _this.placemarkProperties[i];
                    }

                    properties.draggable = _this.draggable;

                    _this.placemark = new ymaps.Placemark(
                        [LocationSelector.center.latitude, LocationSelector.center.longitude],
                        {},
                        properties
                    );

                    map.geoObjects.add(_this.placemark);

                    _this.placemark.events.add('dragend', function(e) {
                        _this.onChangeLocation(_this.placemark.geometry.getCoordinates(), _this.placemark);
                    });
                }

                map.setCenter([latitude, longitude]);
                map.setZoom(zoom);
                _this.placemark.geometry.setCoordinates([latitude, longitude]);
                _this.onChangeLocation([latitude, longitude], _this.placemark);

                deferred.resolve(_this.placemark, map);
            });

            return deferred.promise();
        },

        onChangeLocation: function (coordinates, placemark) {
        }
    }

    LocationSelector.center = {
        latitude: 48.66370039,
        longitude: 67.70768775,
        zoom: 5
    }


    $.fn.locationSelector = function () {
        var first = this.first(),
            selector = new LocationSelector(first);

        selector.onChangeLocation = function (coordinates, placemark) {
            first.trigger('location-change', coordinates, placemark);
        };

        return selector;
    };


})(typeof jQuery != 'undefined' ? jQuery : null, this, this.document);
